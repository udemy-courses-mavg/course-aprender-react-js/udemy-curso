import React, { Component } from 'react';
import BitCoinPriceContainer from './sections/container-component'

class App extends Component {
  render () {
    return (
      <div className="App">
        <h4>Container - Component</h4>
        <BitCoinPriceContainer />
      </div>
    )
  }
}

export default App;