import React, { Component } from 'react';
// import UpdateCycle from './sections/life-cycle/componentWillReceiveProps.js'
// import UpdateCycle from './sections/life-cycle/shouldComponentUpdate.js'
// import UpdateCycle from './sections/life-cycle/componentWillUpdate.js'
// import UpdateCycle from './sections/life-cycle/componentDidUpdate.js'
// import UnmountCycle from './sections/life-cycle/componentWillUnmount.js'
import ErrorCycle from './sections/life-cycle/componentDidCatch.js'

class App extends Component {
  render () {
    return (
      <div className="App">
        {/* <UpdateCycle animal='cat' />
        <UnmountCycle /> */}
        <ErrorCycle />
      </div>
    )
  }
}

export default App;