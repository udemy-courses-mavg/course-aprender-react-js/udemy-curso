import React, { Component } from 'react';

class App extends Component {
  // Todo método tiene un contructor
  /* CONSTRUCTOR POR DEFECTO 
  constructor (..args) {
    super(...args)
  }
  */

  constructor(props){
    console.log('constructor')
    super(props) // este método llama al constructor del componente
    // inicializamos el state de nuestro componente
    this.state = { mensajeInicial: 'mensaje incial' }
    // bindeamos el contexto al método
    // this.handleClick = this.handleClick.bind(this)
  }

  handleClick = () => {
    this.setState({ mensajeInicial: 'mensaje cambiando' })
  }

  render () {
    console.log('render')
    return (
      <div className="App">
        <h4>Ciclo de montaje: constructor</h4>
        {this.state.mensajeInicial}
        <button onClick={this.handleClick}>
          Cambiar mensaje
        </button>        
      </div>
    )
  }
}

export default App;