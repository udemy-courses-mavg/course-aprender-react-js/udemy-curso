import React, { Component } from 'react';
import cars from './data/cars.json'

class App extends Component {
  render () {
    const numbers = [1, 1, 3, 4, 5]

    // return (
    //   <div className="App">
    //     <h4>Trabajando con listas</h4>
    //     {/* {numbers.map(number => {
    //       return <p>Soy el numero {number}</p>
    //     })} */}
    //     {/* Utilizando key en el array */}
    //     {numbers.map((number, index) => {
    //       return <p key={index}>Soy el número {number}</p>
    //     })}
    //   </div>
    // )

    class CardItem extends Component {
      render() {
        const { car, id } = this.props
    
        return (
          <li>
            {/* La prop key no es accesible en nuestro componente. */}
            {/* <p><strong>Key: </strong>{car.key}</p> */}
            {/* Si se necesita esta informaciónse puede utilizar cambiando el nombre de la props */}
            <p><strong>Key: </strong>{id}</p>
            <p><strong>Nombre: </strong>{car.name}</p>
            <p><strong>Marca: </strong>{car.company}</p>
          </li> 
        )
      }
    }
    
    return (
      <div className="App">
        <h4>Trabajando con listas de objetos</h4>
        <ul>
        {
          cars.map(car => { 
            // La prop key debe indicarse en el componente que estamos listando        
            return <CardItem  id={car.id} key={car.id} car={car} />
          })
        }
        </ul>
      </div>
    )
  }
}

export default App;