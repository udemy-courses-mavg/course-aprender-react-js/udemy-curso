import React, { Component } from 'react';

/*
class Button extends Component {
  // constructor(props) {
  //   super(props)
  //   this.borderColor = '#09F'
  // }
  state = { borderColor: '#09F' }

  render() {
    return (
      <button style={{ borderColor: this.state.borderColor, display: 'block' }}>
        {this.props.label}
      </button>
    )
  }
}
*/
class Button extends Component {
  render() {
    return (
      <button style={{ borderColor: this.props.borderColor, display: 'block'}}>
        {this.props.label}
      </button>      
    )
  }
}

Button.defaultProps = {
  borderColor: '#09F'
}

/*
class ButtonDanger extends Button {
  // constructor(props) {
  //   super(props)
  //   this.borderColor = 'red'
  // }
  state = { borderColor: 'red' }
}
*/
class ButtonDanger extends Component {
  render() {
    return <Button borderColor='red' label={this.props.label} />
  }
}

/*
class ButtonWithLegend extends Button {
  render() {
    return (
      <div>
        {super.render()}
        <small>{this.props.legend}</small>
      </div>
    )
  }
}
*/
class ButtonWithLegend extends Component {
  render() {
    return (
      <div>
        <Button borderColor={this.props.borderColor} label={this.props.label} />
        <small>{this.props.legend}</small>
      </div>
    )
  }
}


class App extends Component {
  render () {
    return (
      <div className="App">
        <h4>Composición vs Herencia</h4>
        <Button label='Click aquí' />
        <br/>
        <ButtonDanger label='Cuidado' />
        <br/>
        <ButtonWithLegend 
          label='Botón con leyenda'
          legend='Leyenda del botón'
        />
      </div>
    )
  }
}

export default App;