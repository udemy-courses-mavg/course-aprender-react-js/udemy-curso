import React, { Component } from 'react';
import ConditionalSection from './sections/conditional'
import ConditionalSection2 from './sections/conditional2'

class App extends Component {
  render () {
    return (
      <div className="App">
        <h4>Condicionales</h4>
        <div>
          <ConditionalSection2 />
        </div>
      </div>
    )
  }
}

export default App;