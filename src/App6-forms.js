import React, { Component } from 'react';
import Forms from './sections/forms'

class App extends Component {
  render () {
    return (
      <div className="App">
        <h4>Formularios</h4>
        <div>
          <Forms />
        </div>
      </div>
    )
  }
}

export default App;