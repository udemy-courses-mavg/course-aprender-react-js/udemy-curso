import React, { Component } from 'react';
import PropTypes from 'prop-types';

const ANIMAL_IMAGES = {
  cat: 'https://goo.gl/PoQQXb',
  dolphin: 'https://goo.gl/BbiKCd',
  panda: 'https://goo.gl/oNbtog'
}

const ANIMALS = Object.keys(ANIMAL_IMAGES)

class AnimalImage extends Component {
  state = { src: ANIMAL_IMAGES[this.props.animal] }

// ACTUALIZAMOS EL STATE DE SRC CADA VEZ QUE RECIBIMOS NUEVAS PROPS
  componentWillReceiveProps(nextProps) {
    /* nextProps pueden ser las mismas props que tenemos pero entrará igualmente a este método
    Nuevas props no quiere decir que sean distintas a las que teníamos
    */
    console.log('componentWillReceiveProps')
    console.log(nextProps)
    // if (this.props.animal !== nextProps.animal) {
      this.setState({ src: ANIMAL_IMAGES[nextProps.animal]})
    //   console.log('Change of setState')
    // }
  }

  render(){
    console.log('-> render')
    return (
      <div>
        <p>Selected {this.props.animal}</p>
        <img 
          alt={this.props.animal}
          src={this.state.src}
          width='350'
        />
      </div>
    )
  }
}

AnimalImage.propTypes = {
  // animal: PropTypes.oneOf(['cat', 'dolphin', 'panda'])
  animal: PropTypes.oneOf(ANIMALS)

}

// AnimalImage.defaultProps = {
//   animal: 'dolphin'
// }

export default class CicloDeActualización extends Component {
  state = { animal: 'cat'}
  
  _renderAnimalButton = (animal) => {
    return (
      <button 
        disabled={animal === this.state.animal}
        key={animal} 
        onClick={() => this.setState({ animal })}>
        { animal }
      </button>
    )
  }
  
  render(){
    return (
      <div>
        <h4>Ciclo de Actualización, Ejemplo de: ComponentWillReceiveProps</h4>
        {/* <AnimalImage animal='dolphin' /> */}
        {/* <button onClick={() => this.setState({ animal: 'panda'})}>
          Panda
        </button>
        <button onClick={() => this.setState({ animal: 'cat'})}>
          Cat
        </button>
        <button onClick={() => this.setState({ animal: 'dolphin'})}>
          Dolphin
        </button> */}

        {/* {this._renderAnimalButton('panda')}
        {this._renderAnimalButton('cat')}
        {this._renderAnimalButton('dolphin')} */}
        
        {/* {Object.keys(ANIMAL_IMAGES).map(this._renderAnimalButton)} */}
        {ANIMALS.map(this._renderAnimalButton)}
        <AnimalImage animal={this.state.animal} />
      </div>
    )
  }
}

