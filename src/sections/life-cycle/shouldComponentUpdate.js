// import React, { Component } from 'react';
import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';

const ANIMAL_IMAGES = {
  cat: 'https://goo.gl/PoQQXb',
  dolphin: 'https://goo.gl/BbiKCd',
  panda: 'https://goo.gl/oNbtog'
}

const ANIMALS = Object.keys(ANIMAL_IMAGES)

// class AnimalImage extends Component {
class AnimalImage extends PureComponent {
  state = { src: ANIMAL_IMAGES[this.props.animal] }

  componentWillReceiveProps(nextProps) {
    console.log('1. componentWillReceiveProps')
    console.log(nextProps)
    this.setState({ src: ANIMAL_IMAGES[nextProps.animal]})
  }

  // shouldComponentUpdate(nextProps) {
  //   console.log('2. shouldComponentUpdate')
  //   console.log('anterior: ', this.props.animal)
  //   console.log('nuevo: ', nextProps.animal)
  //   /* Debemos devolver un booleano
  //   Si este método no existe, se devuelve True, por defecto
  //   */
  //   // return true
  //   /* Revisamos si la prop que tiene es distinta a la nueva que esta recibiendo el método. Si son iguales, se retornará un false,
  //   indicando que no es necesario hacer el render, debido a que el false en el shouldComponentUpdate aborta que el ciclo de actualización 
  //   siga su curso. 
  //   */

  //   return this.props.animal !== nextProps.animal
  // }

  render(){
    console.log('-> render')
    return (
      <div>
        <p>Selected {this.props.animal}</p>
        <img 
          alt={this.props.animal}
          src={this.state.src}
          width='300'
        />
      </div>
    )
  }
}

AnimalImage.propTypes = {
  animal: PropTypes.oneOf(ANIMALS)

}

export default class CicloDeActualización extends Component {
  state = { animal: 'cat'}
  
  _renderAnimalButton = (animal) => {
    return (
      <button 
        // disabled={animal === this.state.animal}
        key={animal} 
        onClick={() => this.setState({ animal })}>
        { animal }
      </button>
    )
  }
  
  render(){
    return (
      <div>
        <h4>Ciclo de Actualización, Ejemplo de: ShouldComponentUpdate</h4>
        {ANIMALS.map(this._renderAnimalButton)}
        <AnimalImage animal={this.state.animal} />
      </div>
    )
  }
}

